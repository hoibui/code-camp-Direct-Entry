<?php
class Update
{
    public static function generate($tableName, $columns, $values, $conditon)
    {
      $updateColumns = self::getUpdateColumnsQuery($columns, $values);

      $sql = "UPDATE {$tableName} SET {$updateColumns} WHERE {$conditon}";

      return $sql;
    }

    private function getUpdateColumnsQuery($columns, $values) {
      $updateColumns = "";

      for($i=0; $i<count($columns); $i++) {

        if ( $i < count($columns) - 1 ) {
          $updateColumns .= $columns[$i] . " = '" . $values[$i] . "', ";
        } else {
          $updateColumns .= $columns[$i] . " = '" . $values[$i] . "'";
        }
      }

      return $updateColumns;
    }
}

var_dump(Update::generate("hocsinh",["ten","lop"], ["minh","1A"], "id='1'"));

?>
