<?php

class GameOfLife
{

    protected $rows = 3;
    protected $columns = 3;

    protected $cells;

    public function __construct() {
        $this->cells = $this->generateCells();
    }



    public function getCells() {
        return $this->cells;
    }

    public function nextGeneration() {
        for ($i=0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $check = $this->cells[$i][$j] != $this->caculateAliveOrDead($i, $j);
                $this->cells[$i][$j] = $this->caculateAliveOrDead($i, $j);
                if($check) {
                    break 2;
                }
            }

        }


    }

    private function caculateAliveOrDead($i, $j) {
        $neighbors =
            isset($this->cells[$i + 1][$j + 1]) +
            isset($this->cells[$i + 1][$j - 1]) +
            isset($this->cells[$i + 1][$j]) +
            isset($this->cells[$i - 1][$j + 1]) +
            isset($this->cells[$i - 1][$j - 1]) +
            isset($this->cells[$i - 1][$j]) +
            isset($this->cells[$i][$j + 1]) +
            isset($this->cells[$i][$j - 1]);

        if ($this->cells[$i][$j]==1 && ($neighbors < 2 || $neighbors > 3)) {
          return 0;
        }

        if ($this->cells[$i][$j]==0 && $neighbors == 3) {
          return 1;
        }

        if ($this->cells[$i][$j]==1 && ($neighbors == 2 || $neighbors == 3)) {
            return 1;
        }

        return $this->cells[$i][$j];

    }

    private function generateCells() {
        // $cells = [];
        // for ($i = 0; $i < $this->rows; $i++) {
        //     for ($j = 0; $j < $this->columns; $j++) {
        //         $cells[$i][$j] = rand(1, 0);
        //     }
        // }
        $cells = array(
            array(1,0,1),
            array(0,1,1),
            array(0,0,0)
        );
        return $cells;


    }

}

$newGame = new GameOfLife();
// print_r($newGame->getCells());
$newGame->nextGeneration();
$newGame->nextGeneration();
// $newGame->nextGeneration();
print_r($newGame->getCells());


